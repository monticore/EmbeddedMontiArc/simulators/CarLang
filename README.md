<!-- (c) https://github.com/MontiCore/monticore -->
# Car Modeling Language

## Syntax

Cars support packages. You can define the package of a car via:

```
package my.package.structure;
```

In this case, you would need to place your *.car* file in the directory *{model_path_root}/my/package/structure/* 

All car models have to start with a named car block.

```
car myCar {
    ...
}
```

The name has to match the came of the *.car* file, i.e. according to the exaple above, our car file would need to be named *myCar.car*.

Inside the *car* block, you can define the parameters in any order. However, some parameters reside inside blocks, such as:
```
controller {
    software = PIDAutopilot;
    software_simulator = direct;
    time_model = instant;
}
``` 

And some parameters can have blocks (such as *models* in the following):
```
time_model = models {
    cpu_frequency = 3GHz;
    memory_frequency = 2GHz;
}
```

As can be seen in the example above, (most) parameters will be set following a matching unit. All units also support prefixes/related units (e.g. for grams: mg, g, kg, t).

In some cases, parameters take values in the form of ranges (min:step:max):

```
actuators {
   motor = (-1.5:2.0:3.5);
   ...
}
```

To create a valid car model, you have to specify all available parameters.

### Structure

A *.car* file is structured in the following way (order may differ within a block):

* __car__ (*Name*)
    * __mass__: Number (g)
    * __width__: Number (m)
    * __length__: Number (m)
    * __height__: Number (m)
    * __wheel_radius__: Number (m)
    * __wheel_dist__
        * __front__: Number (m)
        * __back__: Number (m)
        * __to_front__: Number (m)
        * __to_back__: Number (m)
    * __actuators__
        * __motor__: Range (no unit)
        * __steering__: Range (no unit)
        * __brakes__
            * __front__
                * __left__: Range (no unit)
                * __right__: Range (no unit)
            * __back__
                * __left__: Range (no unit)
                * __right__: Range (no unit)
    * __sensors__
        * (List of installed sensors)
    * __controller__
        * __software__: Name
        * __software_simulator__: __direct__ or __emu__
            * (In case of *emu*, and optional): __test_real__
        * __time_model__: __instant__, __constant__ or __models__
            * (For *constant*): __cycle_time__: Number (s)  
            * (For *models*):
            * __cpu_frequency__: Number (Hz)
            * __memory_frequency__: Number (Hz)
            * (Optional:) __cache_IL1__, __cache_DL1__, __cache_L2__ or __cache_L3__
                * __size__: Number (no unit)
                * __read__: Number (no unit)
                * __write__: Number (no unit)
    * __physics_model__: __mass_point__ or __modelica__
    
### Example

```
car default {
    mass = 1800kg;
    width = 2.02712567705637776m;
    length = 4.236423828125m;
    height = 1.19524474896355328m;
    wheel_radius = 0.3334m;
    wheel_dist {
        front = 1.62025m;
        back = 1.62025m;
        to_front = 1.379m;
        to_back = 1.542m;
    }
    
    actuators {
        motor = (-1.5:2.0:3.5);
        brakes {
            front {
                left = (0.0:5.0:5.0);
                right = (0.0:5.0:5.0);
            }
            back {
                left = (0.0:5.0:5.0);
                right = (0.0:5.0:5.0);
            }
        }
        steering = (-0.785398:0.5:0.785398);
    }
    
    sensors {
        SENSOR_VELOCITY;
        SENSOR_STEERING;
        SENSOR_DISTANCE_TO_RIGHT;
        SENSOR_DISTANCE_TO_LEFT;
        SENSOR_LEFT_FRONT_WHEEL_DISTANCE_TO_STREET_SENSOR;
        SENSOR_RIGHT_FRONT_WHEEL_DISTANCE_TO_STREET_SENSOR;
        SENSOR_LEFT_BACK_WHEEL_DISTANCE_TO_STREET_SENSOR;
        SENSOR_RIGHT_BACK_WHEEL_DISTANCE_TO_STREET_SENSOR;
        SENSOR_GPS_COORDINATES;
        SENSOR_CURRENT_SURFACE;
        SENSOR_WEATHER;
        SENSOR_CAMERA;
        SENSOR_COMPASS;
        SENSOR_OBSTACLE;
        SENSOR_VANISHINGPOINT;
        SENSOR_LANE;
        SENSOR_STREETTYPE;
        SENSOR_DAYNIGHT;
        SENSOR_LEFT_FRONT_DISTANCE;
        SENSOR_RIGHT_FRONT_DISTANCE;
    }
    
    controller {
		software = AutopilotAdapter;
		software_simulator = emu {
			test_real;
		}
		time_model = models {
			cpu_frequency = 3GHz;
			memory_frequency = 2500MHz;
			cache_IL1 {
				size = 262144;
				read = 4;
				write = 4;
			}
            ...
			cache_L3 {
				size = 12582912;
				read = 40;
				write = 40;
			}
		}
	}

    physics_model = mass_point;
}
```

## Usage

If car models are referred to in a scenario file (cf. [SimulationLanguage](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/SimulationLanguage)) and exist in the same model path as the scenario, they will be loaded automatically when parsing the scenario file.

If you want to use a car model independently from the SimulationLanguage, you can also use one of the *parse* methods of the class *CarLangTool*.
