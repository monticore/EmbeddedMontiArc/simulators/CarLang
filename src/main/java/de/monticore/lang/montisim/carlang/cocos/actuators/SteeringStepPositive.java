/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.actuators;

import de.monticore.lang.montisim.carlang._ast.ASTSteering;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTSteeringCoCo;
import de.se_rwth.commons.logging.Log;

public class SteeringStepPositive implements CarLangASTSteeringCoCo {
    @Override
    public void check(ASTSteering node) {
        if (!node.getRange().getStepValue().isPositive()) {
            Log.error("Actuator step size must be positive.", node.get_SourcePositionStart());
        }
    }
}
