/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang._symboltable;


import de.monticore.lang.montisim.carlang._ast.*;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.hardware_emulator.config.ControllerConfig;
import de.monticore.lang.montisim.carlang.CarContainer;
import de.monticore.lang.montisim.carlang.ControllerModelVisitor;
import de.monticore.lang.montisim.carlang.PhysicsModel;
import de.monticore.lang.montisim.carlang._visitor.CarLangInheritanceVisitor;
import de.monticore.lang.montisim.carlang._visitor.CarLangVisitor;

import java.time.Duration;
import javax.measure.converter.UnitConverter;
import javax.measure.unit.Unit;

public class CarSymbol extends CarSymbolTOP {

    private CarContainer model;

    public CarSymbol(String name) {
        super(name);
    }

    static class ControllerVisitor1 implements CarLangInheritanceVisitor {
        public String software_name = null;
        public boolean test_real = false;
        public ControllerConfig.EmulatorType soft_sim_type = ControllerConfig.EmulatorType.HARDWARE_EMULATOR;
        @Override
        public void visit(ASTSoftware software_node) {
            software_name = software_node.getName();
        }
        @Override
        public void visit(ASTDirectSoftwareSimulator direct_sim_node) {
            soft_sim_type = ControllerConfig.EmulatorType.DIRECT;
        }
        @Override
        public void visit(ASTEmuSoftwareSimulator emu_sim_node) {
            soft_sim_type = ControllerConfig.EmulatorType.HARDWARE_EMULATOR;
        }
        @Override
        public void visit(ASTTestRealEmuSetting test_real_node) {
            test_real = true;
        }
    };

    public static class ControllerVisitor2 implements CarLangInheritanceVisitor {
        ControllerConfig config;
        public ControllerVisitor2(ControllerConfig config){
            this.config = config;
        }
        @Override
        public void visit(ASTInstantTimeModel instant_node) {
            this.config.set_timemodel_instant();
        }

        @Override
        public void visit(ASTConstantTimeModel constant_node) {
            Unit unit = constant_node.getNumberWithUnit().getUnit();
            UnitConverter converter = unit.getConverterTo(unit.getStandardUnit());
            double duration_seconds = converter.convert(constant_node.getNumberWithUnit().getNumber().orElse(0d));
            this.config.set_timemodel_constant(Duration.ofMillis((long)(1000*duration_seconds)));
        }

        @Override
        public void visit(ASTModelsTimeModel models_node) {
            this.config.set_timemodel_timemodel();
            models_node.accept(new ControllerModelVisitor(this.config));
        }
    };

    private void buildCarModel() {
        model = new CarContainer();
        CarLangVisitor visitor = new CarLangInheritanceVisitor() {
            @Override
            public void visit(ASTMass node) {
                UnitConverter converter = node.getNumberWithUnit().getUnit().getConverterTo(node.getNumberWithUnit().getUnit().getStandardUnit());
                model.setMass(converter.convert(node.getNumberWithUnit().getNumber().orElse(0d)));
            }

            @Override
            public void visit(ASTWidth node) {
                UnitConverter converter = node.getNumberWithUnit().getUnit().getConverterTo(node.getNumberWithUnit().getUnit().getStandardUnit());
                model.setWidth(converter.convert(node.getNumberWithUnit().getNumber().orElse(0d)));
            }

            @Override
            public void visit(ASTLength node) {
                UnitConverter converter = node.getNumberWithUnit().getUnit().getConverterTo(node.getNumberWithUnit().getUnit().getStandardUnit());
                model.setLength(converter.convert(node.getNumberWithUnit().getNumber().orElse(0d)));
            }

            @Override
            public void visit(ASTHeight node) {
                UnitConverter converter = node.getNumberWithUnit().getUnit().getConverterTo(node.getNumberWithUnit().getUnit().getStandardUnit());
                model.setHeight(converter.convert(node.getNumberWithUnit().getNumber().orElse(0d)));
            }

            @Override
            public void visit(ASTWheelRadius node) {
                UnitConverter converter = node.getNumberWithUnit().getUnit().getConverterTo(node.getNumberWithUnit().getUnit().getStandardUnit());
                model.setWheelRadius(converter.convert(node.getNumberWithUnit().getNumber().orElse(0d)));
            }

            @Override
            public void visit(ASTWheelDistFront node) {
                UnitConverter converter = node.getNumberWithUnit().getUnit().getConverterTo(node.getNumberWithUnit().getUnit().getStandardUnit());
                model.setWheelDistFront(converter.convert(node.getNumberWithUnit().getNumber().orElse(0d)));
            }

            @Override
            public void visit(ASTWheelDistBack node) {
                UnitConverter converter = node.getNumberWithUnit().getUnit().getConverterTo(node.getNumberWithUnit().getUnit().getStandardUnit());
                model.setWheelDistBack(converter.convert(node.getNumberWithUnit().getNumber().orElse(0d)));
            }

            @Override
            public void visit(ASTWheelDistToFront node) {
                UnitConverter converter = node.getNumberWithUnit().getUnit().getConverterTo(node.getNumberWithUnit().getUnit().getStandardUnit());
                model.setWheelDistToFront(converter.convert(node.getNumberWithUnit().getNumber().orElse(0d)));
            }

            @Override
            public void visit(ASTWheelDistToBack node) {
                UnitConverter converter = node.getNumberWithUnit().getUnit().getConverterTo(node.getNumberWithUnit().getUnit().getStandardUnit());
                model.setWheelDistToBack(converter.convert(node.getNumberWithUnit().getNumber().orElse(0d)));
            }

            @Override
            public void visit(ASTMotor node) {
                model.setMotorMin(node.getRange().getStartValue().doubleValue());
                model.setMotorMax(node.getRange().getEndValue().doubleValue());
                model.setMotorStep(node.getRange().getStepValue().doubleValue());
            }

            @Override
            public void visit(ASTBrakeFrontLeft node) {
                model.setBrakeFrontLeftMin(node.getRange().getStartValue().doubleValue());
                model.setBrakeFrontLeftMax(node.getRange().getEndValue().doubleValue());
                model.setBrakeFrontLeftStep(node.getRange().getStepValue().doubleValue());
            }

            @Override
            public void visit(ASTBrakeFrontRight node) {
                model.setBrakeFrontRightMin(node.getRange().getStartValue().doubleValue());
                model.setBrakeFrontRightMax(node.getRange().getEndValue().doubleValue());
                model.setBrakeFrontRightStep(node.getRange().getStepValue().doubleValue());
            }

            @Override
            public void visit(ASTBrakeBackLeft node) {
                model.setBrakeBackLeftMin(node.getRange().getStartValue().doubleValue());
                model.setBrakeBackLeftMax(node.getRange().getEndValue().doubleValue());
                model.setBrakeBackLeftStep(node.getRange().getStepValue().doubleValue());
            }

            @Override
            public void visit(ASTBrakeBackRight node) {
                model.setBrakeBackRightMin(node.getRange().getStartValue().doubleValue());
                model.setBrakeBackRightMax(node.getRange().getEndValue().doubleValue());
                model.setBrakeBackRightStep(node.getRange().getStepValue().doubleValue());
            }

            @Override
            public void visit(ASTSteering node) {
                model.setSteeringMin(node.getRange().getStartValue().doubleValue());
                model.setSteeringMax(node.getRange().getEndValue().doubleValue());
                model.setSteeringStep(node.getRange().getStepValue().doubleValue());
            }

            @Override
            public void visit(ASTMassPointModel node) {
                model.setPhysicsModel(PhysicsModel.MASS_POINT);
            }

            @Override
            public void visit(ASTModelicaModel node) {
                model.setPhysicsModel(PhysicsModel.MODELICA);
            }

            @Override
            public void visit(ASTBasicSensor node) {
                for (BusEntry be : BusEntry.values()) {
                    if (be.toString().equalsIgnoreCase(node.getName())) {
                        model.setSensor(be, true);
                    }
                }
            }

            //Controller Entry
            @Override
            public void visit(ASTController controller_node) {
                ControllerVisitor1 controller_visitor1 = new ControllerVisitor1();
                controller_node.accept((CarLangVisitor) controller_visitor1);
                ControllerConfig config = new ControllerConfig(controller_visitor1.soft_sim_type, controller_visitor1.software_name);

                if (controller_visitor1.soft_sim_type == ControllerConfig.EmulatorType.HARDWARE_EMULATOR && controller_visitor1.test_real){
                    config.set_test_real(false);
                }

                //Visit time model
                ControllerVisitor2 controller_visitor2 = new ControllerVisitor2(config);
                controller_node.accept((CarLangVisitor) controller_visitor2);
                model.addController(config);
            }

        };
        // visitor.visit(this.getCarNode().get());
        this.getCarNode().get().accept(visitor);
    }

    /**
     *
     * @return The CarContainer corresponding to the values of the AST connected to this CarSymbol
     */
    public CarContainer getCarContainer() {
        if (model == null) {
            buildCarModel();
        }
        return model;
    }

}
