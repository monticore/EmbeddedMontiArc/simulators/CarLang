/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.basics;

import de.monticore.lang.montisim.carlang._ast.ASTMass;
import de.monticore.lang.montisim.carlang._ast.ASTWidth;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTMassCoCo;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTWidthCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.SI;

public class WidthUnitMeter implements CarLangASTWidthCoCo {

    @Override
    public void check(ASTWidth node) {
        if (!SI.METER.isCompatible(node.getNumberWithUnit().getUnit())) {
            Log.error("The Unit of Width is not compatible with meter (e.g. m, km, etc.).", node.get_SourcePositionStart());
        }
    }

}
