/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.masspoint;

import de.monticore.lang.montisim.carlang._ast.ASTMassPointMass;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTMassPointMassCoCo;
import de.se_rwth.commons.logging.Log;

public class MassPointMassPositive implements CarLangASTMassPointMassCoCo {
    @Override
    public void check(ASTMassPointMass node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("MassPointMass is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("MassPointMass is not present.", node.get_SourcePositionStart());
        }
    }
}
