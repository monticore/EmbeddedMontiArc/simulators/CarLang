/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;

import de.monticore.lang.montisim.carlang._ast.ASTRAMFrequency;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTRAMFrequencyCoCo;
import de.se_rwth.commons.logging.Log;

public class MemoryFrequencyPositive implements CarLangASTRAMFrequencyCoCo {
    @Override
    public void check(ASTRAMFrequency node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("RAM frequency is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("RAM frequency is not present.", node.get_SourcePositionStart());
        }
    }
}
