/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.actuators.brakes;

import de.monticore.lang.montisim.carlang._ast.ASTBrakeFrontLeft;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTBrakeFrontLeftCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.Unit;

public class BrakeFrontLeftUnit implements CarLangASTBrakeFrontLeftCoCo {
    @Override
    public void check(ASTBrakeFrontLeft node) {
        if (!Unit.ONE.isCompatible(node.getRange().getStartUnit()) ||
                !Unit.ONE.isCompatible(node.getRange().getEndUnit()) ||
                !Unit.ONE.isCompatible(node.getRange().getStepUnit()))
        {
            Log.error("Brake actuator values must be unit-less.", node.get_SourcePositionStart());
        }
    }
}
