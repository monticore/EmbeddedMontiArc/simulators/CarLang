/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;


import de.monticore.lang.montisim.carlang._ast.*;
import de.monticore.lang.montisim.carlang._visitor.CarLangInheritanceVisitor;
import de.se_rwth.commons.logging.Log;

public class CacheEntryCountVisitor implements CarLangInheritanceVisitor {

    int count_size = 0;
    int count_read_speed = 0;
    int count_write_speed = 0;
    @Override public void visit(ASTCacheSize node) { count_size++; }
    @Override public void visit(ASTCacheReadSpeed node) { count_read_speed++; }
    @Override public void visit(ASTCacheWriteSpeed node) { count_write_speed++; }

    public void validateEntries() {
        assertHasExactlyOne(count_size, "size");
        assertHasExactlyOne(count_read_speed, "read");
        assertHasExactlyOne(count_write_speed, "write");
    }
    private void assertHasExactlyOne(int count, String param){
        if (count == 0) {
            Log.error("Cache entry does not contain the parameter " + param + ".");
        } else if (count > 1) {
            Log.error("Cache entry contains the parameter " + param + " multiple times.");
        }
    }

}
