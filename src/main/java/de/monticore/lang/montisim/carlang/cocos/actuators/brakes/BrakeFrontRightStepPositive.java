/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.actuators.brakes;

import de.monticore.lang.montisim.carlang._ast.ASTBrakeFrontRight;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTBrakeFrontRightCoCo;
import de.se_rwth.commons.logging.Log;

public class BrakeFrontRightStepPositive implements CarLangASTBrakeFrontRightCoCo {
    @Override
    public void check(ASTBrakeFrontRight node) {
        if (!node.getRange().getStepValue().isPositive()) {
            Log.error("Actuator step size must be positive.", node.get_SourcePositionStart());
        }
    }
}
