/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos;

import de.monticore.lang.montisim.carlang.CarParam;
import de.monticore.lang.montisim.carlang._ast.*;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTCarCoCo;
import de.monticore.lang.montisim.carlang._visitor.CarLangInheritanceVisitor;
import de.se_rwth.commons.logging.Log;

import java.util.HashMap;
import java.util.Map;

public class ParameterDefinitionCount implements CarLangASTCarCoCo {

    private Map<CarParam, Integer> params;

    public ParameterDefinitionCount() {
        params = new HashMap<>();
        for (CarParam p : CarParam.values()) {
            params.put(p, 0);
        }
    }

    @Override
    public void check(ASTCar node) {
        CarLangInheritanceVisitor visitor = new PDCVisitor();
        node.accept(visitor);

        for (Map.Entry<CarParam, Integer> e : params.entrySet()) {
            if (e.getValue() == 0) {
                Log.error("Car File does not contain the parameter " + e.getKey() + ".");
            } else if (e.getValue() > 1) {
                Log.error("Car File contains the parameter " + e.getKey() + " multiple times.");
            }
        }
    }

    private class PDCVisitor implements CarLangInheritanceVisitor {
        @Override
        public void visit(ASTMass node) {
            params.replace(CarParam.MASS, params.get(CarParam.MASS) + 1);
        }
        @Override
        public void visit(ASTWidth node) {
            params.replace(CarParam.WIDTH, params.get(CarParam.WIDTH) + 1);
        }
        @Override
        public void visit(ASTLength node) {
            params.replace(CarParam.LENGTH, params.get(CarParam.LENGTH) + 1);
        }
        @Override
        public void visit(ASTHeight node) {
            params.replace(CarParam.HEIGHT, params.get(CarParam.HEIGHT) + 1);
        }
        @Override
        public void visit(ASTWheelRadius node) {
            params.replace(CarParam.WHEEL_RADIUS, params.get(CarParam.WHEEL_RADIUS) + 1);
        }
        @Override
        public void visit(ASTWheelDistFront node) {
            params.replace(CarParam.WHEEL_DIST_FRONT, params.get(CarParam.WHEEL_DIST_FRONT) + 1);
        }
        @Override
        public void visit(ASTWheelDistBack node) {
            params.replace(CarParam.WHEEL_DIST_BACK, params.get(CarParam.WHEEL_DIST_BACK) + 1);
        }
        @Override
        public void visit(ASTWheelDistToFront node) {
            params.replace(CarParam.WHEEL_DIST_TO_FRONT, params.get(CarParam.WHEEL_DIST_TO_FRONT) + 1);
        }
        @Override
        public void visit(ASTWheelDistToBack node) {
            params.replace(CarParam.WHEEL_DIST_TO_BACK, params.get(CarParam.WHEEL_DIST_TO_BACK) + 1);
        }
        @Override
        public void visit(ASTMotor node) {
            params.replace(CarParam.MOTOR, params.get(CarParam.MOTOR) + 1);
        }
        @Override
        public void visit(ASTBrakeFrontLeft node) {
            params.replace(CarParam.BRAKE_FRONT_LEFT, params.get(CarParam.BRAKE_FRONT_LEFT) + 1);
        }
        @Override
        public void visit(ASTBrakeFrontRight node) {
            params.replace(CarParam.BRAKE_FRONT_RIGHT, params.get(CarParam.BRAKE_FRONT_RIGHT) + 1);
        }
        @Override
        public void visit(ASTBrakeBackLeft node) {
            params.replace(CarParam.BRAKE_BACK_LEFT, params.get(CarParam.BRAKE_BACK_LEFT) + 1);
        }
        @Override
        public void visit(ASTBrakeBackRight node) {
            params.replace(CarParam.BRAKE_BACK_RIGHT, params.get(CarParam.BRAKE_BACK_RIGHT) + 1);
        }
        @Override
        public void visit(ASTSteering node) {
            params.replace(CarParam.STEERING, params.get(CarParam.STEERING) + 1);
        }
        @Override
        public void visit(ASTController node) {
            params.replace(CarParam.CONTROLLER, params.get(CarParam.CONTROLLER) + 1);
        }
        @Override
        public void visit(ASTPhysicsModel node) {
            params.replace(CarParam.PHYSICS_MODEL, params.get(CarParam.PHYSICS_MODEL) + 1);
        }
        @Override
        public void visit(ASTRAMFrequency node) {
            params.replace(CarParam.MEM_FREQ, params.get(CarParam.MEM_FREQ) + 1);
        }
        @Override
        public void visit(ASTCPUFrequency node) {
            params.replace(CarParam.CPU_FREQ, params.get(CarParam.CPU_FREQ) + 1);
        }
    }


}
