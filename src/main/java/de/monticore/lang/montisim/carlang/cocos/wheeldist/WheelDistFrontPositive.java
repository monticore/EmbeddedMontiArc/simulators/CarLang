/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.wheeldist;

import de.monticore.lang.montisim.carlang._ast.ASTWheelDistFront;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTWheelDistFrontCoCo;
import de.se_rwth.commons.logging.Log;

public class WheelDistFrontPositive implements CarLangASTWheelDistFrontCoCo {
    @Override
    public void check(ASTWheelDistFront node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("WheelDistFront is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("WheelDistFront is not present.", node.get_SourcePositionStart());
        }
    }
}
