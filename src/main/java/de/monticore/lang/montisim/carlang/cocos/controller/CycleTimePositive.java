/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;

import de.monticore.lang.montisim.carlang._ast.ASTConstantTimeModel;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTConstantTimeModelCoCo;
import de.se_rwth.commons.logging.Log;

public class CycleTimePositive implements CarLangASTConstantTimeModelCoCo {
    @Override
    public void check(ASTConstantTimeModel node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("cycle_time is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("cycle_time is not present.", node.get_SourcePositionStart());
        }
    }
}
