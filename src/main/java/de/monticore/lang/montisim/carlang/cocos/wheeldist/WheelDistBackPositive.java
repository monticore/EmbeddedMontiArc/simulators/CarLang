/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.wheeldist;

import de.monticore.lang.montisim.carlang._ast.ASTWheelDistBack;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTWheelDistBackCoCo;
import de.se_rwth.commons.logging.Log;

public class WheelDistBackPositive implements CarLangASTWheelDistBackCoCo {
    @Override
    public void check(ASTWheelDistBack node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("WheelDistBack is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("WheelDistBack is not present.", node.get_SourcePositionStart());
        }
    }
}
