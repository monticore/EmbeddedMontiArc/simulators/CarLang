/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.basics;

import de.monticore.lang.montisim.carlang._ast.ASTWidth;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTWidthCoCo;
import de.se_rwth.commons.logging.Log;

public class WidthPositive implements CarLangASTWidthCoCo {

    @Override
    public void check(ASTWidth node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("Width is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("Width is not present.", node.get_SourcePositionStart());
        }
    }

}
