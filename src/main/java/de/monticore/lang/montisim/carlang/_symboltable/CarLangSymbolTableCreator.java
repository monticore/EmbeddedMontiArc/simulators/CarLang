/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang._symboltable;

import de.monticore.lang.montisim.carlang.CoCoCheckerFactory;
import de.monticore.lang.montisim.carlang._ast.ASTCarLangCompilationUnit;
import de.monticore.symboltable.ArtifactScope;
import de.monticore.symboltable.ImportStatement;
import de.monticore.symboltable.MutableScope;
import de.monticore.symboltable.ResolvingConfiguration;
import de.se_rwth.commons.Names;

import java.util.ArrayList;
import java.util.Deque;

public class CarLangSymbolTableCreator extends CarLangSymbolTableCreatorTOP {

    public CarLangSymbolTableCreator(ResolvingConfiguration resolvingConfig, MutableScope enclosingScope) {
        super(resolvingConfig, enclosingScope);
    }

    public CarLangSymbolTableCreator(ResolvingConfiguration resolvingConfig, Deque<MutableScope> scopeStack) {
        super(resolvingConfig, scopeStack);
    }

    @Override
    public void visit(ASTCarLangCompilationUnit node) {
        String packageQualifiedName = Names.getQualifiedName(node.getPackageList());
        // empty list, because cars dont need import statements:
        putOnStack(new ArtifactScope(packageQualifiedName, new ArrayList<ImportStatement>()));
    }

    @Override
    public void endVisit(ASTCarLangCompilationUnit node) {
        super.endVisit(node);
        CoCoCheckerFactory.getChecker().checkAll(node);
    }
}
