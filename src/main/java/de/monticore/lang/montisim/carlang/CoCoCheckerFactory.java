/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang;

import de.monticore.lang.montisim.carlang._cocos.CarLangCoCoChecker;
import de.monticore.lang.montisim.carlang.cocos.*;
import de.monticore.lang.montisim.carlang.cocos.actuators.*;
import de.monticore.lang.montisim.carlang.cocos.basics.*;
import de.monticore.lang.montisim.carlang.cocos.actuators.brakes.*;
import de.monticore.lang.montisim.carlang.cocos.controller.*;
import de.monticore.lang.montisim.carlang.cocos.masspoint.*;
import de.monticore.lang.montisim.carlang.cocos.wheeldist.*;

public class CoCoCheckerFactory {

    /**
     *
     * @return A CoCoChecker for the CarLang with all available CoCos.
     */
    public static CarLangCoCoChecker getChecker() {
        CarLangCoCoChecker checker = new CarLangCoCoChecker();

        checker.addCoCo(new BrakeBackLeftMinMax());
        checker.addCoCo(new BrakeBackLeftStepPositive());
        checker.addCoCo(new BrakeBackLeftUnit());
        checker.addCoCo(new BrakeBackRightMinMax());
        checker.addCoCo(new BrakeBackRightStepPositive());
        checker.addCoCo(new BrakeBackRightUnit());
        checker.addCoCo(new BrakeFrontLeftMinMax());
        checker.addCoCo(new BrakeFrontLeftStepPositive());
        checker.addCoCo(new BrakeFrontLeftUnit());
        checker.addCoCo(new BrakeFrontRightMinMax());
        checker.addCoCo(new BrakeFrontRightStepPositive());
        checker.addCoCo(new BrakeFrontRightUnit());
        checker.addCoCo(new ParameterDefinitionCount());
        checker.addCoCo(new HeightPositive());
        checker.addCoCo(new HeightUnitMeter());
        checker.addCoCo(new LengthPositive());
        checker.addCoCo(new LengthUnitMeter());
        checker.addCoCo(new MassPointAirDragNonNegative());
        checker.addCoCo(new MassPointAirDragUnitOne());
        checker.addCoCo(new MassPointMassPositive());
        checker.addCoCo(new MassPointMassUnitGram());
        checker.addCoCo(new MassPointPressurePositive());
        checker.addCoCo(new MassPointPressureUnitPascal());
        checker.addCoCo(new MassPositive());
        checker.addCoCo(new MassUnitGram());
        checker.addCoCo(new MotorMinMax());
        checker.addCoCo(new MotorStepPositive());
        checker.addCoCo(new MotorUnit());
        checker.addCoCo(new SteeringMinMax());
        checker.addCoCo(new SteeringStepPositive());
        checker.addCoCo(new SteeringUnit());
        checker.addCoCo(new WheelDistBackPositive());
        checker.addCoCo(new WheelDistBackUnitMeter());
        checker.addCoCo(new WheelDistFrontPositive());
        checker.addCoCo(new WheelDistFrontUnitMeter());
        checker.addCoCo(new WheelDistToBackPositive());
        checker.addCoCo(new WheelDistToBackUnitMeter());
        checker.addCoCo(new WheelDistToFrontPositive());
        checker.addCoCo(new WheelDistToFrontUnitMeter());
        checker.addCoCo(new WheelRadiusPositive());
        checker.addCoCo(new WheelRadiusUnitMeter());
        checker.addCoCo(new WidthPositive());
        checker.addCoCo(new WidthUnitMeter());

        checker.addCoCo(new CPUFrequencyPositive());
        checker.addCoCo(new CPUFrequencyUnitHertz());
        checker.addCoCo(new MemoryFrequencyPositive());
        checker.addCoCo(new MemoryFrequencyUnitHertz());
        checker.addCoCo(new CacheReadSpeedPositive());
        checker.addCoCo(new CacheReadSpeedUnitOne());
        checker.addCoCo(new CacheWriteSpeedPositive());
        checker.addCoCo(new CacheWriteSpeedUnitOne());
        checker.addCoCo(new CacheSizePositive());
        checker.addCoCo(new CacheSizeUnitOne());
        checker.addCoCo(new CycleTimePositive());
        checker.addCoCo(new CycleTimeUnitTime());

        checker.addCoCo(new CacheDL1EntryCount());
        checker.addCoCo(new CacheIL1EntryCount());
        checker.addCoCo(new CacheL2EntryCount());
        checker.addCoCo(new CacheL3EntryCount());
        checker.addCoCo(new ControllerDefinitionCount());
        checker.addCoCo(new ModelsTimeModelEntryCount());

        return checker;
    }

}
