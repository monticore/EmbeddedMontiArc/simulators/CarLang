/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.masspoint;

import de.monticore.lang.montisim.carlang._ast.ASTMassPointAirDrag;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTMassPointAirDragCoCo;
import de.se_rwth.commons.logging.Log;

public class MassPointAirDragNonNegative implements CarLangASTMassPointAirDragCoCo {
    @Override
    public void check(ASTMassPointAirDrag node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() < 0) {
                Log.error("The air drag coefficient for the MassPoint Model is not non-negative.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("The air drag coefficient for the MassPoint Model is not present.", node.get_SourcePositionStart());
        }
    }
}
