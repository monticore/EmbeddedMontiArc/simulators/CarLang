/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;

import de.monticore.lang.montisim.carlang._ast.ASTCacheWriteSpeed;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTCacheWriteSpeedCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.Unit;

public class CacheWriteSpeedUnitOne implements CarLangASTCacheWriteSpeedCoCo {
    @Override
    public void check(ASTCacheWriteSpeed node) {
        if (node.getNumberWithUnit().getUnit() != Unit.ONE) {
            Log.error("Cache write speed does not accept units (cpu cycle count).", node.get_SourcePositionStart());
        }
    }
}
