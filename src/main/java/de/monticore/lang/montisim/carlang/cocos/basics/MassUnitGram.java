/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.basics;

import de.monticore.lang.montisim.carlang._ast.ASTMass;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTMassCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.SI;

public class MassUnitGram implements CarLangASTMassCoCo {

    @Override
    public void check(ASTMass node) {
        if (!SI.GRAM.isCompatible(node.getNumberWithUnit().getUnit())) {
            Log.error("The Unit of Mass is not compatible with gram (e.g. g, kg, etc.).", node.get_SourcePositionStart());
        }
    }

}
