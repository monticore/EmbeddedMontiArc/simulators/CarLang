/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.wheeldist;

import de.monticore.lang.montisim.carlang._ast.ASTWheelDistFront;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTWheelDistFrontCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.SI;

public class WheelDistFrontUnitMeter implements CarLangASTWheelDistFrontCoCo {
    @Override
    public void check(ASTWheelDistFront node) {
        if (!SI.METER.isCompatible(node.getNumberWithUnit().getUnit())) {
            Log.error("The Unit of WheelDistFront is not compatible with meter (e.g. m, km, etc.).", node.get_SourcePositionStart());
        }
    }
}
