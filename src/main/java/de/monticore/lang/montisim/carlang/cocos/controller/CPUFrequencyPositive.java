/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;

import de.monticore.lang.montisim.carlang._ast.ASTCPUFrequency;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTCPUFrequencyCoCo;
import de.se_rwth.commons.logging.Log;

public class CPUFrequencyPositive implements CarLangASTCPUFrequencyCoCo {
    @Override
    public void check(ASTCPUFrequency node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("CPU frequency is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("CPU frequency is not present.", node.get_SourcePositionStart());
        }
    }
}
