/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.basics;

import de.monticore.lang.montisim.carlang._ast.ASTLength;
import de.monticore.lang.montisim.carlang._ast.ASTWidth;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTLengthCoCo;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTWidthCoCo;
import de.se_rwth.commons.logging.Log;

public class LengthPositive implements CarLangASTLengthCoCo {

    @Override
    public void check(ASTLength node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("Length is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("Length is not present.", node.get_SourcePositionStart());
        }
    }

}
