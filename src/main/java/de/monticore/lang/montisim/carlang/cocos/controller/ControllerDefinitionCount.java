/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;


import de.monticore.lang.montisim.carlang._ast.*;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTControllerCoCo;
import de.monticore.lang.montisim.carlang._visitor.CarLangInheritanceVisitor;
import de.se_rwth.commons.logging.Log;

public class ControllerDefinitionCount implements CarLangASTControllerCoCo {
    int count_software = 0;
    int count_software_simulator = 0;
    int count_timemodels = 0;


    @Override
    public void check(ASTController node) {
        CarLangInheritanceVisitor visitor = new CDCVisitor();
        node.accept(visitor);

        assertHasExactlyOne(count_software, "software");
        assertHasExactlyOne(count_software_simulator, "software_simulator");
        assertHasExactlyOne(count_timemodels, "time_model");
    }

    private void assertHasExactlyOne(int count, String param){
        if (count == 0) {
            Log.error("Controller entry does not contain the parameter " + param + ".");
        } else if (count > 1) {
            Log.error("Controller entry contains the parameter " + param + " multiple times.");
        }
    }

    private class CDCVisitor implements CarLangInheritanceVisitor {
        @Override public void visit(ASTSoftware node) { count_software++; }
        @Override public void visit(ASTSoftwareSimulator node) { count_software_simulator++; }
        @Override public void visit(ASTTimeModel node) { count_timemodels++; }
    }


}
