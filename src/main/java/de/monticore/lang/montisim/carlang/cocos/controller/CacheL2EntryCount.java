/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;


import de.monticore.lang.montisim.carlang._ast.*;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTL2CacheCoCo;

public class CacheL2EntryCount implements CarLangASTL2CacheCoCo {
    @Override
    public void check(ASTL2Cache node) {
        CacheEntryCountVisitor visitor = new CacheEntryCountVisitor();
        node.accept(visitor);
        visitor.validateEntries();
    }
}
