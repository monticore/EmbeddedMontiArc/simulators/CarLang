/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.basics;

import de.monticore.lang.montisim.carlang._ast.ASTMass;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTMassCoCo;
import de.se_rwth.commons.logging.Log;

public class MassPositive implements CarLangASTMassCoCo {

    @Override
    public void check(ASTMass node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("Mass is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("Mass is not present.", node.get_SourcePositionStart());
        }
    }

}
