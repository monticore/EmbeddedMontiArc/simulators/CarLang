/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.actuators.brakes;

import de.monticore.lang.montisim.carlang._ast.ASTBrakeFrontRight;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTBrakeFrontRightCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.Unit;

public class BrakeFrontRightUnit implements CarLangASTBrakeFrontRightCoCo {
    @Override
    public void check(ASTBrakeFrontRight node) {
        if (!Unit.ONE.isCompatible(node.getRange().getStartUnit()) ||
                !Unit.ONE.isCompatible(node.getRange().getEndUnit()) ||
                !Unit.ONE.isCompatible(node.getRange().getStepUnit()))
        {
            Log.error("Brake actuator values must be unit-less.", node.get_SourcePositionStart());
        }
    }
}
