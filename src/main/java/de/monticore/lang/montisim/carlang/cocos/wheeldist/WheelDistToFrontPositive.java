/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.wheeldist;

import de.monticore.lang.montisim.carlang._ast.ASTWheelDistToFront;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTWheelDistToFrontCoCo;
import de.se_rwth.commons.logging.Log;

public class WheelDistToFrontPositive implements CarLangASTWheelDistToFrontCoCo {
    @Override
    public void check(ASTWheelDistToFront node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("WheelDistToFront is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("WheelDistToFront is not present.", node.get_SourcePositionStart());
        }
    }
}
