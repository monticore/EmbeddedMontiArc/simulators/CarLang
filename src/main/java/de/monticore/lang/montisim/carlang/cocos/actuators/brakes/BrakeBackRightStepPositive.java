/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.actuators.brakes;

import de.monticore.lang.montisim.carlang._ast.ASTBrakeBackRight;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTBrakeBackRightCoCo;
import de.se_rwth.commons.logging.Log;

public class BrakeBackRightStepPositive implements CarLangASTBrakeBackRightCoCo {
    @Override
    public void check(ASTBrakeBackRight node) {
        if (!node.getRange().getStepValue().isPositive()) {
            Log.error("Actuator step size must be positive.", node.get_SourcePositionStart());
        }
    }
}
