/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.basics;

import de.monticore.lang.montisim.carlang._ast.ASTHeight;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTHeightCoCo;
import de.se_rwth.commons.logging.Log;

public class HeightPositive implements CarLangASTHeightCoCo {
    @Override
    public void check(ASTHeight node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("Height is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("Height is not present.", node.get_SourcePositionStart());
        }
    }
}
