/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang;

/**
 * Defines all the settable parameters in a car model.
 */
public enum CarParam {
    MASS,
    WIDTH,
    LENGTH,
    HEIGHT,
    WHEEL_RADIUS,
    WHEEL_DIST_FRONT,
    WHEEL_DIST_BACK,
    WHEEL_DIST_TO_FRONT,
    WHEEL_DIST_TO_BACK,
    MOTOR,
    BRAKE_FRONT_LEFT,
    BRAKE_FRONT_RIGHT,
    BRAKE_BACK_LEFT,
    BRAKE_BACK_RIGHT,
    STEERING,
    CONTROLLER,
    CPU_FREQ,
    MEM_FREQ,
    PHYSICS_MODEL
}
