/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;

import de.monticore.lang.montisim.carlang._ast.ASTCacheSize;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTCacheSizeCoCo;
import de.se_rwth.commons.logging.Log;

public class CacheSizePositive implements CarLangASTCacheSizeCoCo {
    @Override
    public void check(ASTCacheSize node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("Cache size is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("Cache size is not present.", node.get_SourcePositionStart());
        }
    }
}
