/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;

import de.monticore.lang.montisim.carlang._ast.ASTCacheWriteSpeed;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTCacheWriteSpeedCoCo;
import de.se_rwth.commons.logging.Log;

public class CacheWriteSpeedPositive implements CarLangASTCacheWriteSpeedCoCo {
    @Override
    public void check(ASTCacheWriteSpeed node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("Cache write speed is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("Cache write speed is not present.", node.get_SourcePositionStart());
        }
    }
}
