/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.masspoint;

import de.monticore.lang.montisim.carlang._ast.ASTMassPointPressure;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTMassPointPressureCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.SI;

public class MassPointPressureUnitPascal implements CarLangASTMassPointPressureCoCo {
    @Override
    public void check(ASTMassPointPressure node) {
        if (!SI.PASCAL.isCompatible(node.getNumberWithUnit().getUnit())) {
            Log.error("The Unit of MassPointMass is not compatible with gram (e.g. g, kg, etc.).", node.get_SourcePositionStart());
        }
    }
}
