/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.wheeldist;

import de.monticore.lang.montisim.carlang._ast.ASTWheelDistToBack;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTWheelDistToBackCoCo;
import de.se_rwth.commons.logging.Log;

public class WheelDistToBackPositive implements CarLangASTWheelDistToBackCoCo {
    @Override
    public void check(ASTWheelDistToBack node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("WheelDistToBack is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("WheelDistToBack is not present.", node.get_SourcePositionStart());
        }
    }
}
