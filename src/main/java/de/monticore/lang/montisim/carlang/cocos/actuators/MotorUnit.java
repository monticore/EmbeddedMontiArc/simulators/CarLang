/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.actuators;

import de.monticore.lang.montisim.carlang._ast.ASTMotor;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTMotorCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.Unit;

public class MotorUnit implements CarLangASTMotorCoCo {
    @Override
    public void check(ASTMotor node) {
        if (!Unit.ONE.isCompatible(node.getRange().getStartUnit()) ||
            !Unit.ONE.isCompatible(node.getRange().getEndUnit()) ||
            !Unit.ONE.isCompatible(node.getRange().getStepUnit()))
        {
            Log.error("Motor actuator values must be unit-less.", node.get_SourcePositionStart());
        }
    }
}
