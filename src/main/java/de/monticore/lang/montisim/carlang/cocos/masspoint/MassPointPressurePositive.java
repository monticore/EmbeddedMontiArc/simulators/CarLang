/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.masspoint;

import de.monticore.lang.montisim.carlang._ast.ASTMassPointPressure;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTMassPointPressureCoCo;
import de.se_rwth.commons.logging.Log;

public class MassPointPressurePositive implements CarLangASTMassPointPressureCoCo {
    @Override
    public void check(ASTMassPointPressure node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("MassPointPressure is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("MassPointPressure is not present.", node.get_SourcePositionStart());
        }
    }
}
