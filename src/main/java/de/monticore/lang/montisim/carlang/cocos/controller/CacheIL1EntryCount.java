/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;


import de.monticore.lang.montisim.carlang._ast.*;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTIL1CacheCoCo;

public class CacheIL1EntryCount implements CarLangASTIL1CacheCoCo {
    @Override
    public void check(ASTIL1Cache node) {
        CacheEntryCountVisitor visitor = new CacheEntryCountVisitor();
        node.accept(visitor);
        visitor.validateEntries();
    }
}
