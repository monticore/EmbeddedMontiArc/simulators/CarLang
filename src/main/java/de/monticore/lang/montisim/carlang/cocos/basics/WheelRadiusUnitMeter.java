/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.basics;

import de.monticore.lang.montisim.carlang._ast.ASTWheelRadius;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTWheelRadiusCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.SI;

public class WheelRadiusUnitMeter implements CarLangASTWheelRadiusCoCo {
    @Override
    public void check(ASTWheelRadius node) {
        if (!SI.METER.isCompatible(node.getNumberWithUnit().getUnit())) {
            Log.error("The Unit of WheelRadius is not compatible with meter (e.g. m, km, etc.).", node.get_SourcePositionStart());
        }
    }
}
