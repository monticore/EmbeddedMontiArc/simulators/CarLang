/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang;

import com.google.gson.Gson;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.hardware_emulator.config.ControllerConfig;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;


/**
 * A class that provides easy access to the values represented by the AST of a car model.
 */
public class CarContainer {

    private double mass;
    private double width;
    private double length;
    private double height;
    private double wheelRadius;

    private double wheelDistFront;
    private double wheelDistBack;
    private double wheelDistToFront;
    private double wheelDistToBack;

    private double motorMin;
    private double motorMax;
    private double motorStep;

    private double brakeFrontLeftMin;
    private double brakeFrontLeftMax;
    private double brakeFrontLeftStep;

    private double brakeFrontRightMin;
    private double brakeFrontRightMax;
    private double brakeFrontRightStep;

    private double brakeBackLeftMin;
    private double brakeBackLeftMax;
    private double brakeBackLeftStep;

    private double brakeBackRightMin;
    private double brakeBackRightMax;
    private double brakeBackRightStep;

    private double steeringMin;
    private double steeringMax;
    private double steeringStep;


    private Map<BusEntry, Boolean> sensors = new HashMap<>();
    private List<ControllerConfig> controllers = new ArrayList<>();

    private PhysicsModel physicsModel;


    public CarContainer() { }

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWheelRadius() {
        return wheelRadius;
    }

    public void setWheelRadius(double wheelRadius) {
        this.wheelRadius = wheelRadius;
    }

    public double getWheelDistFront() {
        return wheelDistFront;
    }

    public void setWheelDistFront(double wheelDistFront) {
        this.wheelDistFront = wheelDistFront;
    }

    public double getWheelDistBack() {
        return wheelDistBack;
    }

    public void setWheelDistBack(double wheelDistBack) {
        this.wheelDistBack = wheelDistBack;
    }

    public double getWheelDistToFront() {
        return wheelDistToFront;
    }

    public void setWheelDistToFront(double wheelDistToFront) {
        this.wheelDistToFront = wheelDistToFront;
    }

    public double getWheelDistToBack() {
        return wheelDistToBack;
    }

    public void setWheelDistToBack(double wheelDistToBack) {
        this.wheelDistToBack = wheelDistToBack;
    }

    public double getMotorMin() {
        return motorMin;
    }

    public void setMotorMin(double motorMin) {
        this.motorMin = motorMin;
    }

    public double getMotorMax() {
        return motorMax;
    }

    public void setMotorMax(double motorMax) {
        this.motorMax = motorMax;
    }

    public double getMotorStep() {
        return motorStep;
    }

    public void setMotorStep(double motorStep) {
        this.motorStep = motorStep;
    }

    public double getBrakeFrontLeftMin() {
        return brakeFrontLeftMin;
    }

    public void setBrakeFrontLeftMin(double brakeFrontLeftMin) {
        this.brakeFrontLeftMin = brakeFrontLeftMin;
    }

    public double getBrakeFrontLeftMax() {
        return brakeFrontLeftMax;
    }

    public void setBrakeFrontLeftMax(double brakeFrontLeftMax) {
        this.brakeFrontLeftMax = brakeFrontLeftMax;
    }

    public double getBrakeFrontLeftStep() {
        return brakeFrontLeftStep;
    }

    public void setBrakeFrontLeftStep(double brakeFrontLeftStep) {
        this.brakeFrontLeftStep = brakeFrontLeftStep;
    }

    public double getBrakeFrontRightMin() {
        return brakeFrontRightMin;
    }

    public void setBrakeFrontRightMin(double brakeFrontRightMin) {
        this.brakeFrontRightMin = brakeFrontRightMin;
    }

    public double getBrakeFrontRightMax() {
        return brakeFrontRightMax;
    }

    public void setBrakeFrontRightMax(double brakeFrontRightMax) {
        this.brakeFrontRightMax = brakeFrontRightMax;
    }

    public double getBrakeFrontRightStep() {
        return brakeFrontRightStep;
    }

    public void setBrakeFrontRightStep(double brakeFrontRightStep) {
        this.brakeFrontRightStep = brakeFrontRightStep;
    }

    public double getBrakeBackLeftMin() {
        return brakeBackLeftMin;
    }

    public void setBrakeBackLeftMin(double brakeBackLeftMin) {
        this.brakeBackLeftMin = brakeBackLeftMin;
    }

    public double getBrakeBackLeftMax() {
        return brakeBackLeftMax;
    }

    public void setBrakeBackLeftMax(double brakeBackLeftMax) {
        this.brakeBackLeftMax = brakeBackLeftMax;
    }

    public double getBrakeBackLeftStep() {
        return brakeBackLeftStep;
    }

    public void setBrakeBackLeftStep(double brakeBackLeftStep) {
        this.brakeBackLeftStep = brakeBackLeftStep;
    }

    public double getBrakeBackRightMin() {
        return brakeBackRightMin;
    }

    public void setBrakeBackRightMin(double brakeBackRightMin) {
        this.brakeBackRightMin = brakeBackRightMin;
    }

    public double getBrakeBackRightMax() {
        return brakeBackRightMax;
    }

    public void setBrakeBackRightMax(double brakeBackRightMax) {
        this.brakeBackRightMax = brakeBackRightMax;
    }

    public double getBrakeBackRightStep() {
        return brakeBackRightStep;
    }

    public void setBrakeBackRightStep(double brakeBackRightStep) {
        this.brakeBackRightStep = brakeBackRightStep;
    }

    public double getSteeringMin() {
        return steeringMin;
    }

    public void setSteeringMin(double steeringMin) {
        this.steeringMin = steeringMin;
    }

    public double getSteeringMax() {
        return steeringMax;
    }

    public void setSteeringMax(double steeringMax) {
        this.steeringMax = steeringMax;
    }

    public double getSteeringStep() {
        return steeringStep;
    }

    public void setSteeringStep(double steeringStep) {
        this.steeringStep = steeringStep;
    }

    public PhysicsModel getPhysicsModel() {
        return physicsModel;
    }

    public void setPhysicsModel(PhysicsModel physicsModel) {
        this.physicsModel = physicsModel;
    }

    public void setSensor(BusEntry sensor, boolean value) {
        sensors.put(sensor, value);
    }

    public boolean hasSensor(BusEntry sensor) {
        return sensors.getOrDefault(sensor, false);
    }

    public void addController(ControllerConfig config){
        controllers.add(config);
    }

    public List<ControllerConfig> getControllers(){
        return controllers;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static CarContainer fromJson(String json) {
        return new Gson().fromJson(json, CarContainer.class);
    }

}
