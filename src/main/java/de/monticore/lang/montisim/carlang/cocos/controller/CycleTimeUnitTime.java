/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;

import de.monticore.lang.montisim.carlang._ast.ASTConstantTimeModel;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTConstantTimeModelCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.SI;

public class CycleTimeUnitTime implements CarLangASTConstantTimeModelCoCo {
    @Override
    public void check(ASTConstantTimeModel node) {
        if (!SI.SECOND.isCompatible(node.getNumberWithUnit().getUnit())) {
            Log.error("The Unit of cycle_time is not a duration unit (e.g. ms, s, etc.).", node.get_SourcePositionStart());
        }
    }
}
