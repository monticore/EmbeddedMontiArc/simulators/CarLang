/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;

import de.monticore.lang.montisim.carlang._ast.ASTRAMFrequency;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTRAMFrequencyCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.SI;

public class MemoryFrequencyUnitHertz implements CarLangASTRAMFrequencyCoCo {
    @Override
    public void check(ASTRAMFrequency node) {
        if (!SI.HERTZ.isCompatible(node.getNumberWithUnit().getUnit())) {
            Log.error("The Unit of RAM frequency is not compatible with hertz (e.g. MHz, GHz, etc.).", node.get_SourcePositionStart());
        }
    }
}
