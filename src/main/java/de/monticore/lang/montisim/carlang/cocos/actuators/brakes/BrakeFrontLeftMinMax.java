/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.actuators.brakes;

import de.monticore.lang.montisim.carlang._ast.ASTBrakeFrontLeft;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTBrakeFrontLeftCoCo;
import de.se_rwth.commons.logging.Log;

public class BrakeFrontLeftMinMax implements CarLangASTBrakeFrontLeftCoCo {
    @Override
    public void check(ASTBrakeFrontLeft node) {
        if (node.getRange().getStartValue().compareTo(node.getRange().getEndValue()) >= 0) {
            Log.error("The minimum value for a brake actuator may not be greater or equal to the maximum value.", node.get_SourcePositionStart());
        }
    }
}
