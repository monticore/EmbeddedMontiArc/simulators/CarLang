/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;

import de.monticore.lang.montisim.carlang._ast.ASTCacheSize;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTCacheSizeCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.Unit;

public class CacheSizeUnitOne implements CarLangASTCacheSizeCoCo {
    @Override
    public void check(ASTCacheSize node) {
        if (node.getNumberWithUnit().getUnit() != Unit.ONE) {
            Log.error("Cache size does not accept units.", node.get_SourcePositionStart());
        }
    }
}
