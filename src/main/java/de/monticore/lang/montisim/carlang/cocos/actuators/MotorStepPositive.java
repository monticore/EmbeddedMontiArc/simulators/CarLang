/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.actuators;

import de.monticore.lang.montisim.carlang._ast.ASTMotor;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTMotorCoCo;
import de.se_rwth.commons.logging.Log;

public class MotorStepPositive  implements CarLangASTMotorCoCo {
    @Override
    public void check(ASTMotor node) {
        if (!node.getRange().getStepValue().isPositive()) {
            Log.error("Actuator step size must be positive.", node.get_SourcePositionStart());
        }
    }
}
