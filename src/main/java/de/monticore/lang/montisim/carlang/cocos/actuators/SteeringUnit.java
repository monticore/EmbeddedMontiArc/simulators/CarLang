/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.actuators;

import de.monticore.lang.montisim.carlang._ast.ASTSteering;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTSteeringCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.SI;
import javax.measure.unit.Unit;

public class SteeringUnit implements CarLangASTSteeringCoCo {
    @Override
    public void check(ASTSteering node) {
        if (!SI.RADIAN.isCompatible(node.getRange().getStartUnit()) ||
                !SI.RADIAN.isCompatible(node.getRange().getEndUnit()) ||
                !SI.RADIAN.isCompatible(node.getRange().getStepUnit()))
        {
            Log.error("Steering actuator values must be of a unit compatible with radian.", node.get_SourcePositionStart());
        }
    }
}
