/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;

import de.monticore.lang.montisim.carlang._ast.ASTCacheReadSpeed;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTCacheReadSpeedCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.Unit;

public class CacheReadSpeedUnitOne implements CarLangASTCacheReadSpeedCoCo {
    @Override
    public void check(ASTCacheReadSpeed node) {
        if (node.getNumberWithUnit().getUnit() != Unit.ONE) {
            Log.error("Cache read speed does not accept units (cpu cycle count).", node.get_SourcePositionStart());
        }
    }
}
