/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.actuators.brakes;

import de.monticore.lang.montisim.carlang._ast.ASTBrakeBackLeft;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTBrakeBackLeftCoCo;
import de.se_rwth.commons.logging.Log;

public class BrakeBackLeftStepPositive implements CarLangASTBrakeBackLeftCoCo {
    @Override
    public void check(ASTBrakeBackLeft node) {
        if (!node.getRange().getStepValue().isPositive()) {
            Log.error("Actuator step size must be positive.", node.get_SourcePositionStart());
        }
    }
}
