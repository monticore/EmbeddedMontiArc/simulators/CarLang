/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.wheeldist;

import de.monticore.lang.montisim.carlang._ast.ASTWheelDistToBack;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTWheelDistToBackCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.SI;

public class WheelDistToBackUnitMeter implements CarLangASTWheelDistToBackCoCo {
    @Override
    public void check(ASTWheelDistToBack node) {
        if (!SI.METER.isCompatible(node.getNumberWithUnit().getUnit())) {
            Log.error("The Unit of WheelDistToFront is not compatible with meter (e.g. m, km, etc.).", node.get_SourcePositionStart());
        }
    }
}
