/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.basics;

import de.monticore.lang.montisim.carlang._ast.ASTWheelRadius;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTWheelRadiusCoCo;
import de.se_rwth.commons.logging.Log;

public class WheelRadiusPositive implements CarLangASTWheelRadiusCoCo {
    @Override
    public void check(ASTWheelRadius node) {
        if (node.getNumberWithUnit().getNumber().isPresent()) {
            if (node.getNumberWithUnit().getNumber().get() <= 0) {
                Log.error("WheelRadius is not positive.", node.get_SourcePositionStart());
            }
        } else {
            Log.error("WheelRadius is not present.", node.get_SourcePositionStart());
        }
    }
}
