/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang;

import de.monticore.ModelingLanguage;
import de.monticore.io.paths.ModelPath;
import de.monticore.lang.montisim.carlang._ast.ASTCarLangCompilationUnit;
import de.monticore.lang.montisim.carlang._parser.CarLangParser;
import de.monticore.lang.montisim.carlang._symboltable.CarLangLanguage;
import de.monticore.lang.montisim.carlang._symboltable.CarSymbol;
import de.monticore.symboltable.GlobalScope;
import de.monticore.symboltable.Scope;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

public class CarLangTool {

    /**
     * Parses a car file into a CarSymbol
     * @param modelPath The model path containing the car model file
     * @param modelName The name of the car model
     * @return The CarSumbol linked to the AST of the parsed car model
     */
    public static Optional<CarSymbol> parse(Path modelPath, String modelName) {
        ModelingLanguage carLang = new CarLangLanguage("CarLang", "car") {};
        ModelPath _modelPath = new ModelPath(modelPath);
        Scope globalScope = new GlobalScope(_modelPath, carLang);

        return globalScope.resolve(modelName, CarSymbol.KIND);
    }

    /**
     * Parses a model provided as a string containing the concrete syntax of the model
     * @param model the concrete syntax of the model
     */
    public static Optional<ASTCarLangCompilationUnit> parse(String model) throws IOException {
        CarLangParser parser = new CarLangParser();
        return parser.parse_String(model);
    }

}
