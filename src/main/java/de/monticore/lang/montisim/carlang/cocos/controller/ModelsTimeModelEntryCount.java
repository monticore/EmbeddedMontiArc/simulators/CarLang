/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.controller;


import de.monticore.lang.montisim.carlang._ast.*;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTModelsTimeModelCoCo;
import de.monticore.lang.montisim.carlang._visitor.CarLangInheritanceVisitor;
import de.se_rwth.commons.logging.Log;

public class ModelsTimeModelEntryCount implements CarLangASTModelsTimeModelCoCo {
    int count_cpu = 0;
    int count_mem = 0;
    int count_DL1 = 0;
    int count_IL1 = 0;
    int count_L2 = 0;
    int count_L3 = 0;


    @Override
    public void check(ASTModelsTimeModel node) {
        CarLangInheritanceVisitor visitor = new ModelsDCVisitor();
        node.accept(visitor);

        assertHasExactlyOne(count_cpu, "cpu_frequency");
        assertHasExactlyOne(count_mem, "memory_frequency");
        assertHasMaxOne(count_DL1, "cache_DL1");
        assertHasMaxOne(count_IL1, "cache_IL1");
        assertHasMaxOne(count_L2, "cache_L2");
        assertHasMaxOne(count_L3, "cache_L3");
    }

    private void assertHasExactlyOne(int count, String param){
        if (count == 0) {
            Log.error("time_model=models{} entry does not contain the parameter " + param + ".");
        } else assertHasMaxOne(count, param);
    }
    private void assertHasMaxOne(int count, String param){
        if (count > 1) {
            Log.error("time_model=models{} entry contains the parameter " + param + " multiple times.");
        }
    }

    private class ModelsDCVisitor implements CarLangInheritanceVisitor {
        @Override public void visit(ASTCPUFrequency node) { count_cpu++; }
        @Override public void visit(ASTRAMFrequency node) { count_mem++; }
        @Override public void visit(ASTDL1Cache node) { count_DL1++; }
        @Override public void visit(ASTIL1Cache node) { count_IL1++; }
        @Override public void visit(ASTL2Cache node) { count_L2++; }
        @Override public void visit(ASTL3Cache node) { count_L3++; }
    }


}
