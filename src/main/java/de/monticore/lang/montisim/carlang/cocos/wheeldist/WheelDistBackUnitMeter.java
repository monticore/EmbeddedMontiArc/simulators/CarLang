/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.wheeldist;

import de.monticore.lang.montisim.carlang._ast.ASTWheelDistBack;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTWheelDistBackCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.SI;

public class WheelDistBackUnitMeter implements CarLangASTWheelDistBackCoCo {
    @Override
    public void check(ASTWheelDistBack node) {
        if (!SI.METER.isCompatible(node.getNumberWithUnit().getUnit())) {
            Log.error("The Unit of WheelDistBack is not compatible with meter (e.g. m, km, etc.).", node.get_SourcePositionStart());
        }
    }
}
