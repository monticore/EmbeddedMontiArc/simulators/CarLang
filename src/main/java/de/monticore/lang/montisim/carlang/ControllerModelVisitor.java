/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang;


import de.rwth.monticore.EmbeddedMontiArc.simulators.hardware_emulator.config.ControllerConfig;
import de.monticore.lang.montisim.carlang._visitor.CarLangInheritanceVisitor;
import de.monticore.lang.montisim.carlang._ast.*;

import javax.measure.unit.Unit;
import javax.measure.converter.UnitConverter;


public class ControllerModelVisitor implements CarLangInheritanceVisitor {
    public static class CacheConfigVisitor implements CarLangInheritanceVisitor {
        private long size = 0; //Cache Size in bytes
        private long read_ticks = 0; // Number of CPU cycles for a read action
        private long write_ticks = 0; //Number of CPU cycles for a write action

        @Override
        public void visit(ASTCacheSize node) {
            size = node.getNumberWithUnit().getNumber().orElse(0d).longValue();
        }

        @Override
        public void visit(ASTCacheReadSpeed node) {
            read_ticks = node.getNumberWithUnit().getNumber().orElse(0d).longValue();
        }

        @Override
        public void visit(ASTCacheWriteSpeed node) {
            write_ticks = node.getNumberWithUnit().getNumber().orElse(0d).longValue();
        }

        public ControllerConfig.CacheOption getCacheConfig(){
            return new ControllerConfig.CacheOption(size, read_ticks, write_ticks);
        } 
    }



    ControllerConfig config;
    public ControllerModelVisitor(ControllerConfig config){
        this.config = config;
    }

    @Override
    public void visit(ASTCPUFrequency node) {
        Unit unit = node.getNumberWithUnit().getUnit();
        UnitConverter converter = unit.getConverterTo(unit.getStandardUnit());
        double cpu_frequency = converter.convert(node.getNumberWithUnit().getNumber().orElse(0d));
        config.set_cpu_frequency((long) cpu_frequency);
    }

    @Override
    public void visit(ASTRAMFrequency node) {
        Unit unit = node.getNumberWithUnit().getUnit();
        UnitConverter converter = unit.getConverterTo(unit.getStandardUnit());
        double ram_frequency = converter.convert(node.getNumberWithUnit().getNumber().orElse(0d));
        config.set_memory_frequency((long) ram_frequency);
    }

    @Override
    public void visit(ASTIL1Cache node) {
        CacheConfigVisitor visitor = new CacheConfigVisitor();
        node.accept(visitor);
        config.set_cache_IL1(visitor.getCacheConfig());
    }
    @Override
    public void visit(ASTDL1Cache node) {
        CacheConfigVisitor visitor = new CacheConfigVisitor();
        node.accept(visitor);
        config.set_cache_DL1(visitor.getCacheConfig());
    }
    @Override
    public void visit(ASTL2Cache node) {
        CacheConfigVisitor visitor = new CacheConfigVisitor();
        node.accept(visitor);
        config.set_cache_L2(visitor.getCacheConfig());
    }
    @Override
    public void visit(ASTL3Cache node) {
        CacheConfigVisitor visitor = new CacheConfigVisitor();
        node.accept(visitor);
        config.set_cache_L3(visitor.getCacheConfig());
    }
}