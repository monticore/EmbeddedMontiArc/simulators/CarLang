/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang.cocos.masspoint;

import de.monticore.lang.montisim.carlang._ast.ASTMassPointAirDrag;
import de.monticore.lang.montisim.carlang._cocos.CarLangASTMassPointAirDragCoCo;
import de.se_rwth.commons.logging.Log;

import javax.measure.unit.SI;
import javax.measure.unit.Unit;

public class MassPointAirDragUnitOne implements CarLangASTMassPointAirDragCoCo {
    @Override
    public void check(ASTMassPointAirDrag node) {
        if (!Unit.ONE.isCompatible(node.getNumberWithUnit().getUnit())) {
            Log.error("The air drag coefficient for the MassPoint Model should not have a unit.", node.get_SourcePositionStart());
        }
    }
}
