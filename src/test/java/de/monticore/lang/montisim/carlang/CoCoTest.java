/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang;

import de.monticore.lang.montisim.carlang._parser.CarLangParser;
import de.monticore.lang.montisim.carlang.cocos.actuators.*;
import de.monticore.lang.montisim.carlang.cocos.basics.*;
import de.monticore.lang.montisim.carlang.cocos.actuators.brakes.*;
import de.monticore.lang.montisim.carlang.cocos.controller.*;
import de.monticore.lang.montisim.carlang.cocos.masspoint.*;
import de.monticore.lang.montisim.carlang.cocos.wheeldist.*;
import de.se_rwth.commons.logging.Log;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;


public class CoCoTest {

    @Before
    public void setUp() {
        Log.getFindings().clear();
        Log.enableFailQuick(false);
    }

    @Test
    public void testValid() throws IOException {
        CarLangParser parser = new CarLangParser();

        new BrakeBackLeftMinMax().check(parser.parse_StringBrakeBackLeft("left = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new BrakeBackLeftStepPositive().check(parser.parse_StringBrakeBackLeft("left = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new BrakeBackLeftUnit().check(parser.parse_StringBrakeBackLeft("left = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new BrakeBackRightMinMax().check(parser.parse_StringBrakeBackRight("right = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new BrakeBackRightStepPositive().check(parser.parse_StringBrakeBackRight("right = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new BrakeBackRightUnit().check(parser.parse_StringBrakeBackRight("right = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new BrakeFrontLeftMinMax().check(parser.parse_StringBrakeFrontLeft("left = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new BrakeFrontLeftStepPositive().check(parser.parse_StringBrakeFrontLeft("left = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new BrakeFrontLeftUnit().check(parser.parse_StringBrakeFrontLeft("left = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new BrakeFrontRightMinMax().check(parser.parse_StringBrakeFrontRight("right = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new BrakeFrontRightStepPositive().check(parser.parse_StringBrakeFrontRight("right = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new BrakeFrontRightUnit().check(parser.parse_StringBrakeFrontRight("right = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new HeightPositive().check(parser.parse_StringHeight("height = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new HeightUnitMeter().check(parser.parse_StringHeight("height = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new LengthPositive().check(parser.parse_StringLength("length = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new LengthUnitMeter().check(parser.parse_StringLength("length = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new MassPointAirDragNonNegative().check(parser.parse_StringMassPointAirDrag("air_drag = 2;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new MassPointAirDragUnitOne().check(parser.parse_StringMassPointAirDrag("air_drag = 2;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new MassPointMassPositive().check(parser.parse_StringMassPointMass("mass = 2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new MassPointMassUnitGram().check(parser.parse_StringMassPointMass("mass = 2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new MassPointPressurePositive().check(parser.parse_StringMassPointPressure("pressure = 2Pa;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new MassPointPressureUnitPascal().check(parser.parse_StringMassPointPressure("pressure = 2Pa;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new MassPositive().check(parser.parse_StringMass("mass = 2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new MassUnitGram().check(parser.parse_StringMass("mass = 2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new MotorMinMax().check(parser.parse_StringMotor("motor = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new MotorStepPositive().check(parser.parse_StringMotor("motor = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new MotorUnit().check(parser.parse_StringMotor("motor = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        // TODO ParameterDefinitionCount
        /*new ParameterDefinitionCount().check(parser.parse_String("").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();*/

        new SteeringMinMax().check(parser.parse_StringSteering("steering = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new SteeringStepPositive().check(parser.parse_StringSteering("steering = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new SteeringUnit().check(parser.parse_StringSteering("steering = (2:1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new WheelDistBackPositive().check(parser.parse_StringWheelDistBack("back = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new WheelDistBackUnitMeter().check(parser.parse_StringWheelDistBack("back = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new WheelDistFrontPositive().check(parser.parse_StringWheelDistFront("front = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new WheelDistFrontUnitMeter().check(parser.parse_StringWheelDistFront("front = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new WheelDistToBackPositive().check(parser.parse_StringWheelDistToBack("to_back = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new WheelDistToBackUnitMeter().check(parser.parse_StringWheelDistToBack("to_back = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new WheelDistToFrontPositive().check(parser.parse_StringWheelDistToFront("to_front = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new WheelDistToFrontUnitMeter().check(parser.parse_StringWheelDistToFront("to_front = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new WheelRadiusPositive().check(parser.parse_StringWheelRadius("wheel_radius = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new WheelRadiusUnitMeter().check(parser.parse_StringWheelRadius("wheel_radius = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new WidthPositive().check(parser.parse_StringWidth("width = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new WidthUnitMeter().check(parser.parse_StringWidth("width = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new CPUFrequencyPositive().check(parser.parse_StringCPUFrequency("cpu_frequency = 2GHz;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new CPUFrequencyUnitHertz().check(parser.parse_StringCPUFrequency("cpu_frequency = 2GHz;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new MemoryFrequencyPositive().check(parser.parse_StringRAMFrequency("memory_frequency = 2GHz;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new MemoryFrequencyUnitHertz().check(parser.parse_StringRAMFrequency("memory_frequency = 2GHz;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new CacheReadSpeedPositive().check(parser.parse_StringCacheReadSpeed("read = 4;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new CacheReadSpeedUnitOne().check(parser.parse_StringCacheReadSpeed("read = 4;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new CacheWriteSpeedPositive().check(parser.parse_StringCacheWriteSpeed("write = 4;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new CacheWriteSpeedUnitOne().check(parser.parse_StringCacheWriteSpeed("write = 4;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new CacheSizePositive().check(parser.parse_StringCacheSize("size = 2048;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new CacheSizeUnitOne().check(parser.parse_StringCacheSize("size = 4;").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new CycleTimePositive().check(parser.parse_StringConstantTimeModel("constant { cycle_time = 4ms; }").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new CycleTimeUnitTime().check(parser.parse_StringConstantTimeModel("constant { cycle_time = 4ms; }").get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        //Enough to check for one Cache Type
        new CacheL2EntryCount().check(parser.parse_StringL2Cache(
                "cache_L2 { size=1024; read=4; write=5; }"
        ).get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new ModelsTimeModelEntryCount().check(parser.parse_StringModelsTimeModel(
                "models { cpu_frequency = 4GHz; memory_frequency = 4GHz; cache_IL1 { size=1024; read=4; write=5; } }"
        ).get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();

        new ControllerDefinitionCount().check(parser.parse_StringController(
                "controller { software=random;software_simulator=direct;time_model=instant; }"
        ).get());
        Assert.assertEquals(Log.getErrorCount(), 0);
        Log.getFindings().clear();
    }

    @Test
    public void testInvalid() throws IOException {
        CarLangParser parser = new CarLangParser();

        new BrakeBackLeftMinMax().check(parser.parse_StringBrakeBackLeft("left = (3:1:2);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new BrakeBackLeftStepPositive().check(parser.parse_StringBrakeBackLeft("left = (2:-1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new BrakeBackLeftUnit().check(parser.parse_StringBrakeBackLeft("left = (2mg:1mg:3mg);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new BrakeBackRightMinMax().check(parser.parse_StringBrakeBackRight("right = (3:1:2);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new BrakeBackRightStepPositive().check(parser.parse_StringBrakeBackRight("right = (2:-1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new BrakeBackRightUnit().check(parser.parse_StringBrakeBackRight("right = (2mg:1mg:3mg);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new BrakeFrontLeftMinMax().check(parser.parse_StringBrakeFrontLeft("left = (3:1:2);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new BrakeFrontLeftStepPositive().check(parser.parse_StringBrakeFrontLeft("left = (2:-1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new BrakeFrontLeftUnit().check(parser.parse_StringBrakeFrontLeft("left = (2mg:1mg:3mg);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new BrakeFrontRightMinMax().check(parser.parse_StringBrakeFrontRight("right = (3:1:2);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new BrakeFrontRightStepPositive().check(parser.parse_StringBrakeFrontRight("right = (2:-1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new BrakeFrontRightUnit().check(parser.parse_StringBrakeFrontRight("right = (2mg:1mg:3mg);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new HeightPositive().check(parser.parse_StringHeight("height = -2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new HeightUnitMeter().check(parser.parse_StringHeight("height = 2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new LengthPositive().check(parser.parse_StringLength("length = -2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new LengthUnitMeter().check(parser.parse_StringLength("length = 2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new MassPointAirDragNonNegative().check(parser.parse_StringMassPointAirDrag("air_drag = -2;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new MassPointAirDragUnitOne().check(parser.parse_StringMassPointAirDrag("air_drag = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new MassPointMassPositive().check(parser.parse_StringMassPointMass("mass = -2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new MassPointMassUnitGram().check(parser.parse_StringMassPointMass("mass = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new MassPointPressurePositive().check(parser.parse_StringMassPointPressure("pressure = -2Pa;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new MassPointPressureUnitPascal().check(parser.parse_StringMassPointPressure("pressure = 2km;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new MassPositive().check(parser.parse_StringMass("mass = -2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new MassUnitGram().check(parser.parse_StringMass("mass = 2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new MotorMinMax().check(parser.parse_StringMotor("motor = (3:1:2);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new MotorStepPositive().check(parser.parse_StringMotor("motor = (2:-1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new MotorUnit().check(parser.parse_StringMotor("motor = (2mg:1mg:3mg);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        // TODO ParameterDefinitionCount
        /*new ParameterDefinitionCount().check(parser.parse_String("").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();*/

        new SteeringMinMax().check(parser.parse_StringSteering("steering = (3:1:2);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new SteeringStepPositive().check(parser.parse_StringSteering("steering = (2:-1:3);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new SteeringUnit().check(parser.parse_StringSteering("steering = (2mg:1mg:3mg);").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new WheelDistBackPositive().check(parser.parse_StringWheelDistBack("back = -2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new WheelDistBackUnitMeter().check(parser.parse_StringWheelDistBack("back = 2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new WheelDistFrontPositive().check(parser.parse_StringWheelDistFront("front = -2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new WheelDistFrontUnitMeter().check(parser.parse_StringWheelDistFront("front = 2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new WheelDistToBackPositive().check(parser.parse_StringWheelDistToBack("to_back = -2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new WheelDistToBackUnitMeter().check(parser.parse_StringWheelDistToBack("to_back = 2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new WheelDistToFrontPositive().check(parser.parse_StringWheelDistToFront("to_front = -2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new WheelDistToFrontUnitMeter().check(parser.parse_StringWheelDistToFront("to_front = 2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new WheelRadiusPositive().check(parser.parse_StringWheelRadius("wheel_radius = -2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new WheelRadiusUnitMeter().check(parser.parse_StringWheelRadius("wheel_radius = 2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new WidthPositive().check(parser.parse_StringWidth("width = -2m;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new WidthUnitMeter().check(parser.parse_StringWidth("width = 2kg;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new CPUFrequencyPositive().check(parser.parse_StringCPUFrequency("cpu_frequency = -2GHz;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new CPUFrequencyUnitHertz().check(parser.parse_StringCPUFrequency("cpu_frequency = 2km;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new MemoryFrequencyPositive().check(parser.parse_StringRAMFrequency("memory_frequency = -2GHz;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new MemoryFrequencyUnitHertz().check(parser.parse_StringRAMFrequency("memory_frequency = 2km;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new CacheReadSpeedPositive().check(parser.parse_StringCacheReadSpeed("read = -4;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new CacheReadSpeedUnitOne().check(parser.parse_StringCacheReadSpeed("read = 4s;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new CacheWriteSpeedPositive().check(parser.parse_StringCacheWriteSpeed("write = -4;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new CacheWriteSpeedUnitOne().check(parser.parse_StringCacheWriteSpeed("write = 4s;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new CacheSizePositive().check(parser.parse_StringCacheSize("size = -4;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new CacheSizeUnitOne().check(parser.parse_StringCacheSize("size = 4Hz;").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new CycleTimePositive().check(parser.parse_StringConstantTimeModel("constant { cycle_time = -4ms; }").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        new CycleTimeUnitTime().check(parser.parse_StringConstantTimeModel("constant { cycle_time = 4km; }").get());
        Assert.assertEquals(Log.getErrorCount(), 1);
        Log.getFindings().clear();

        //Enough to check for one Cache Type
        new CacheL2EntryCount().check(parser.parse_StringL2Cache(
                "cache_L2 { write=2; size=1024; write=5; size=1; }"
        ).get());
        Assert.assertEquals(Log.getErrorCount(), 3);
        Log.getFindings().clear();

        new ModelsTimeModelEntryCount().check(parser.parse_StringModelsTimeModel(
                "models { memory_frequency = 4GHz; cache_IL1 { size=1024; read=4; write=5; } cache_IL1 { size=1024; read=4; write=5; }}"
        ).get());
        Assert.assertEquals(Log.getErrorCount(), 2);
        Log.getFindings().clear();

        new ControllerDefinitionCount().check(parser.parse_StringController(
                "controller { software=random;time_model=instant; software=a2;}"
        ).get());
        Assert.assertEquals(Log.getErrorCount(), 2);
        Log.getFindings().clear();

    }

}
