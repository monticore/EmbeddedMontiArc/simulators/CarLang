/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.monticore.lang.montisim.carlang;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.monticore.ModelingLanguage;
import de.monticore.io.paths.ModelPath;
import de.monticore.lang.montisim.carlang._symboltable.CarLangLanguage;
import de.monticore.lang.montisim.carlang._symboltable.CarSymbol;
import de.monticore.symboltable.GlobalScope;
import de.monticore.symboltable.Scope;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Paths;
import java.util.Optional;

public class CarContainerCreationTest {

    private static final String MODEL_PATH = "src/test/resources";
    private static final String MODEL_NAME = "cmct";
    private static final double DELTA = 0.01d;

    private final CarContainer expected = new CarContainer();
    private static final String expected_controller =
              "software=Autopilot\n"
            + "mode=emu\n"
            + "time_model=models\n"
            + "os=auto\n"
            + "test_real=try\n"
            + "cpu_frequency=3000000000\n"
            + "memory_frequency=2000000000\n"
            + "cache_IL1=1024,4,3\n"
            + "cache_L3=262144,10,9\n";

    @Before
    public void setUp() {
        expected.setMass(1800d);
        expected.setWidth(2d);
        expected.setLength(4d);
        expected.setHeight(1d);
        expected.setWheelRadius(0.3d);
        expected.setWheelDistFront(1.6d);
        expected.setWheelDistBack(1.6d);
        expected.setWheelDistToFront(1.3d);
        expected.setWheelDistToBack(1.5d);

        expected.setMotorMin(-1.5d);
        expected.setMotorMax(3.5d);
        expected.setMotorStep(2.0d);

        expected.setBrakeFrontLeftMin(0d);
        expected.setBrakeFrontLeftMax(5d);
        expected.setBrakeFrontLeftStep(5d);

        expected.setBrakeFrontRightMin(0d);
        expected.setBrakeFrontRightMax(5d);
        expected.setBrakeFrontRightStep(5d);

        expected.setBrakeBackLeftMin(0d);
        expected.setBrakeBackLeftMax(5d);
        expected.setBrakeBackLeftStep(5d);

        expected.setBrakeBackRightMin(0d);
        expected.setBrakeBackRightMax(5d);
        expected.setBrakeBackRightStep(5d);

        expected.setSteeringMin(-1d);
        expected.setSteeringMax(1d);
        expected.setSteeringStep(0.5d);

        expected.setSensor(BusEntry.SENSOR_VELOCITY, true);
        expected.setSensor(BusEntry.SENSOR_STEERING, true);
        expected.setSensor(BusEntry.SENSOR_GPS_COORDINATES, true);

        expected.setPhysicsModel(PhysicsModel.MASS_POINT);
    }

    @Test
    public void test() {
        ModelingLanguage carLang = new CarLangLanguage("CarLang", "car") {};
        ModelPath modelPath = new ModelPath(Paths.get(MODEL_PATH));
        Scope globalScope = new GlobalScope(modelPath, carLang);

        Optional<CarSymbol> cmct = globalScope.resolve(MODEL_NAME, CarSymbol.KIND);
        Assert.assertTrue("cmct could not be resolved", cmct.isPresent());

        // Optional<Symbol> cmct2 = globalScope.resolve("blub.cmct2", CarSymbol.KIND);
        // System.out.println("" + cmct2.isPresent());

        // CoCoCheckerFactory.getChecker().checkAll(cmct.get().getCarNode().get());

        CarContainer actual = cmct.get().getCarContainer();

        Assert.assertEquals("Mass incorrect", expected.getMass(), actual.getMass(), DELTA);
        Assert.assertEquals("Width incorrect", expected.getWidth(), actual.getWidth(), DELTA);
        Assert.assertEquals("Length incorrect", expected.getLength(), actual.getLength(), DELTA);
        Assert.assertEquals("Height incorrect", expected.getHeight(), actual.getHeight(), DELTA);
        Assert.assertEquals("WheelRadius incorrect", expected.getWheelRadius(), actual.getWheelRadius(), DELTA);

        Assert.assertEquals("WheelDistFront incorrect", expected.getWheelDistFront(), actual.getWheelDistFront(), DELTA);
        Assert.assertEquals("WheelDistBack incorrect", expected.getWheelDistBack(), actual.getWheelDistBack(), DELTA);
        Assert.assertEquals("WheelDistToFront incorrect", expected.getWheelDistToFront(), actual.getWheelDistToFront(), DELTA);
        Assert.assertEquals("WheelDistToBack incorrect", expected.getWheelDistToBack(), actual.getWheelDistToBack(), DELTA);

        Assert.assertEquals("MotorMin incorrect", expected.getMotorMin(), actual.getMotorMin(), DELTA);
        Assert.assertEquals("MotorMax incorrect", expected.getMotorMax(), actual.getMotorMax(), DELTA);
        Assert.assertEquals("MotorStep incorrect", expected.getMotorStep(), actual.getMotorStep(), DELTA);

        Assert.assertEquals("BrakeFrontLeftMin incorrect", expected.getBrakeFrontLeftMin(), actual.getBrakeFrontLeftMin(), DELTA);
        Assert.assertEquals("BrakeFrontLeftMax incorrect", expected.getBrakeFrontLeftMax(), actual.getBrakeFrontLeftMax(), DELTA);
        Assert.assertEquals("BrakeFrontLeftStep incorrect", expected.getBrakeFrontLeftStep(), actual.getBrakeFrontLeftStep(), DELTA);

        Assert.assertEquals("BrakeFrontRightMin incorrect", expected.getBrakeFrontRightMin(), actual.getBrakeFrontRightMin(), DELTA);
        Assert.assertEquals("BrakeFrontRightMax incorrect", expected.getBrakeFrontRightMax(), actual.getBrakeFrontRightMax(), DELTA);
        Assert.assertEquals("BrakeFrontRightStep incorrect", expected.getBrakeFrontRightStep(), actual.getBrakeFrontRightStep(), DELTA);

        Assert.assertEquals("BrakeBackLeftMin incorrect", expected.getBrakeBackLeftMin(), actual.getBrakeBackLeftMin(), DELTA);
        Assert.assertEquals("BrakeBackLeftMax incorrect", expected.getBrakeBackLeftMax(), actual.getBrakeBackLeftMax(), DELTA);
        Assert.assertEquals("BrakeBackLeftStep incorrect", expected.getBrakeBackLeftStep(), actual.getBrakeBackLeftStep(), DELTA);

        Assert.assertEquals("BrakeBackRightMin incorrect", expected.getBrakeBackRightMin(), actual.getBrakeBackRightMin(), DELTA);
        Assert.assertEquals("BrakeBackRightMax incorrect", expected.getBrakeBackRightMax(), actual.getBrakeBackRightMax(), DELTA);
        Assert.assertEquals("BrakeBackRightStep incorrect", expected.getBrakeBackRightStep(), actual.getBrakeBackRightStep(), DELTA);

        Assert.assertEquals("SteeringMin incorrect", expected.getSteeringMin(), actual.getSteeringMin(), DELTA);
        Assert.assertEquals("SteeringMax incorrect", expected.getSteeringMax(), actual.getSteeringMax(), DELTA);
        Assert.assertEquals("SteeringStep incorrect", expected.getSteeringStep(), actual.getSteeringStep(), DELTA);

        Assert.assertEquals("Missing controller entry", 1, actual.getControllers().size());
        Assert.assertEquals("Controller config incorrect", expected_controller, actual.getControllers().get(0).get_config_string());

        for (BusEntry be : BusEntry.values()) {
            Assert.assertEquals(be.toString() + "-Sensor mismatch", expected.hasSensor(be), actual.hasSensor(be));
        }

        Assert.assertEquals("PhysicsModel incorrect", expected.getPhysicsModel(), actual.getPhysicsModel());
    }

}
